package com.cardpaymentsystem.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.web.servlet.MockMvc;

import com.cardpaymentsystem.models.User;
import com.cardpaymentsystem.repository.UserRepository;
import com.cardpaymentsystem.security.jwt.JwtUtils;
import com.cardpaymentsystem.security.services.UserDetailsServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(AuthController.class)
class AuthControllerTest {

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@MockBean
	AuthenticationManager authenticationManager;

	@MockBean
	PasswordEncoder encoder;

	@MockBean
	JwtUtils jwtUtils;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

//	@Test
//	void testAuthenticateUser() throws Exception {
//		User user1 = new User(4L, "Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, encoder.encode("vijay@123"));
//		
//		mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
//				 .content(objectMapper.writeValueAsString(user1))).andExpect(status().isOk());
//
//	}

	@Test
	void testRegisterUser() throws Exception {
		User user1 = new User(4L, "Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");

		Mockito.when(userDetailsServiceImpl.existsByUsername(user1.getUsername())).thenReturn(false);
		Mockito.when(userDetailsServiceImpl.existsByEmail(user1.getEmail())).thenReturn(false);
		Mockito.when(userDetailsServiceImpl.save(user1)).thenReturn(user1);

		mockMvc.perform(post("/api/auth/signup").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user1))).andExpect(status().isOk()).andDo(print());

	}

	@Test
	void testRegisterUser1() throws Exception {
		User user1 = new User(4L, "Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");

		Mockito.when(userDetailsServiceImpl.existsByUsername(user1.getUsername())).thenReturn(true);
		Mockito.when(userDetailsServiceImpl.existsByEmail(user1.getEmail())).thenReturn(false);
		Mockito.when(userDetailsServiceImpl.save(user1)).thenReturn(user1);

		mockMvc.perform(post("/api/auth/signup").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user1))).andExpect(status().isBadRequest()).andDo(print());

	}

	@Test
	void testRegisterUser2() throws Exception {
		User user1 = new User(4L, "Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");

		Mockito.when(userDetailsServiceImpl.existsByUsername(user1.getUsername())).thenReturn(false);
		Mockito.when(userDetailsServiceImpl.existsByEmail(user1.getEmail())).thenReturn(true);
		Mockito.when(userDetailsServiceImpl.save(user1)).thenReturn(user1);

		mockMvc.perform(post("/api/auth/signup").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user1))).andExpect(status().isBadRequest()).andDo(print());

	}

	@Test
	void testUpdateUser() throws Exception {
		User user1 = new User(4L, "Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");

		Mockito.when(userDetailsServiceImpl.existsByUsername(user1.getUsername())).thenReturn(true);
		Mockito.when(userDetailsServiceImpl.existsByEmail(user1.getEmail())).thenReturn(true);
		Mockito.when(userDetailsServiceImpl.findByEmail(user1.getEmail())).thenReturn(user1);
		Mockito.when(userDetailsServiceImpl.save(user1)).thenReturn(user1);

		mockMvc.perform(put("/api/auth/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user1)))
				.andExpect(status().isOk()).andDo(print());

	}

	@Test
	void testUpdateUser2() throws Exception {
		User user1 = new User(4L, "Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");

		Mockito.when(userDetailsServiceImpl.existsByUsername(user1.getUsername())).thenReturn(false);
		Mockito.when(userDetailsServiceImpl.existsByEmail(user1.getEmail())).thenReturn(true);
		Mockito.when(userDetailsServiceImpl.findByEmail(user1.getEmail())).thenReturn(user1);
		Mockito.when(userDetailsServiceImpl.save(user1)).thenReturn(user1);

		mockMvc.perform(put("/api/auth/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user1)))
				.andExpect(status().isBadRequest()).andDo(print());

	}

	@Test
	void testUpdateUser3() throws Exception {
		User user1 = new User(4L, "Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");

		Mockito.when(userDetailsServiceImpl.existsByUsername(user1.getUsername())).thenReturn(true);
		Mockito.when(userDetailsServiceImpl.existsByEmail(user1.getEmail())).thenReturn(false);
		Mockito.when(userDetailsServiceImpl.findByEmail(user1.getEmail())).thenReturn(user1);
		Mockito.when(userDetailsServiceImpl.save(user1)).thenReturn(user1);

		mockMvc.perform(put("/api/auth/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user1)))
				.andExpect(status().isBadRequest()).andDo(print());

	}

}
