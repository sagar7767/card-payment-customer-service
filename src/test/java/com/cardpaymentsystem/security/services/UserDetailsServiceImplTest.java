package com.cardpaymentsystem.security.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;

import com.cardpaymentsystem.models.User;
import com.cardpaymentsystem.repository.UserRepository;

class UserDetailsServiceImplTest {

	@Mock
	private UserRepository userRepository;

//	@Mock
//	private UserDetailsImpl userDetailsImpl;

	@InjectMocks
	private UserDetailsServiceImpl userService;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
	}

	User user1 = new User(4L, "Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234L, "vijay@123");
	User user2 = new User(5L, "Chetan", 21, "chetan@gmail.com", "Bagalkot", 9876543210L, "chetan@123");

	@Test
	void testLoadUserByUsername() {
		when(userRepository.findByUsername(user1.getUsername())).thenReturn(Optional.of(user1));

		UserDetails user = userService.loadUserByUsername(user1.getUsername());

		assertThat(user).isNotNull();

	}

//	@Test
//	void testLoadUserByUsername1(){
//		
//		when(userRepository.findByUsername("Sagar Lokare")).thenReturn(Optional.of(user1));
//		//when(userService.loadUserByUsername(any(String.class))).thenReturn(uuser1);
//
//		UserDetails user = userService.loadUserByUsername(user1.getUsername());
//		assertThrows(UsernameNotFoundException.class, () -> {
//			userService.loadUserByUsername(user.getUsername());
//		});
//
//	}

	@Test
	void testFindByUsername() {
		when(userRepository.findByUsername(user1.getUsername())).thenReturn(Optional.of(user1));

		Optional<User> user = userService.findByUsername(user1.getUsername());

		assertThat(user).isNotNull();
	}

	@Test
	void testFindByEmail() {
		when(userRepository.findByEmail(user1.getEmail())).thenReturn(user1);

		User user = userService.findByEmail(user1.getEmail());

		assertThat(user).isNotNull();

	}

	@Test
	void testExistsByUsername() {
		when(userRepository.existsByUsername(user1.getUsername())).thenReturn(true);
		Boolean actual = userService.existsByUsername(user1.getUsername());

		assertEquals(true, actual);
		verify(userRepository, times(1)).existsByUsername(user1.getUsername());

	}

	@Test
	void testExistsByEmail() {
		when(userRepository.existsByEmail(user1.getEmail())).thenReturn(true);
		Boolean actual = userService.existsByEmail(user1.getEmail());

		assertEquals(true, actual);
		verify(userRepository, times(1)).existsByEmail(user1.getEmail());

	}

	@Test
	void testSave() {
		userService.save(user1);

		verify(userRepository, times(1)).save(user1);

	}

}
