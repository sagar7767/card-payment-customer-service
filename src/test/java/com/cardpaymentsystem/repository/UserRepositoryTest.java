package com.cardpaymentsystem.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.cardpaymentsystem.models.User;

@DataJpaTest
class UserRepositoryTest {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testFindByEmail() {
		User user = new User("Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");
		entityManager.persist(user);

		User payments = userRepository.findByEmail(user.getEmail());

		assertThat(payments).isEqualTo(user);

	}

	@Test
	void testExistsByUsername() {
		User user = new User("Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");
		entityManager.persist(user);

		Boolean payments = userRepository.existsByUsername(user.getUsername());

		assertThat(payments).isEqualTo(true);

	}

	@Test
	void testExistsByEmail() {
		User user = new User("Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");
		entityManager.persist(user);

		Boolean payments = userRepository.existsByEmail(user.getEmail());

		assertThat(payments).isEqualTo(true);

	}

	@Test
	void testFindByUsername() {
		User user = new User("Vijay", 21, "vijay@gmail.com", "Vijayapur", 12341234, "vijay@123");
		entityManager.persist(user);

		Optional<User> payments = userRepository.findByUsername(user.getUsername());
		User existingUser = new User();
		String nameWeWanted = "";
		if (payments.isPresent()) {
			existingUser = payments.get();
			nameWeWanted = existingUser.getUsername();
		}
		assertThat(nameWeWanted).isEqualTo(user.getUsername());

	}

}
