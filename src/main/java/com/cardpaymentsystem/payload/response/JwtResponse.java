package com.cardpaymentsystem.payload.response;

//import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private int age;
	private String email;
	private String address;
	private long phoneNumber;
	//private List<String> roles;

	public JwtResponse(String accessToken, Long id, String username, int age, String email, String address,
			long phoneNumber) {//, List<String> roles) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.age = age;
		this.email = email;
		this.address = address;
		this.phoneNumber = phoneNumber;
		//this.roles = roles;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

//	public List<String> getRoles() {
//		return roles;
//	}
}