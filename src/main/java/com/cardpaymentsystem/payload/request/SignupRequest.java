package com.cardpaymentsystem.payload.request;

import java.util.Set;

import javax.validation.constraints.*;

public class SignupRequest {
	@NotBlank
	@Size(min = 3, max = 20)
	private String username;

	// added
//	@NotBlank
//	@Size(max = 2)
	private int age;

	@NotBlank
	@Size(max = 50)
	@Email
	private String email;

	// added
//	@NotBlank
//	@Size(max = 100)
	private String address;

	// added
//	@NotBlank
//	@Size(max = 10)
	private long phoneNumber;

//	private Set<String> role;

	@NotBlank
	@Size(min = 6, max = 40)
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

//	public Set<String> getRole() {
//		return this.role;
//	}
//
//	public void setRole(Set<String> role) {
//		this.role = role;
//	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}