package com.cardpaymentsystem.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.cardpaymentsystem.models.ERole;
//import com.cardpaymentsystem.models.Role;
import com.cardpaymentsystem.models.User;
import com.cardpaymentsystem.payload.request.LoginRequest;
import com.cardpaymentsystem.payload.request.SignupRequest;
import com.cardpaymentsystem.payload.response.JwtResponse;
import com.cardpaymentsystem.payload.response.MessageResponse;
//import com.cardpaymentsystem.repository.RoleRepository;
import com.cardpaymentsystem.repository.UserRepository;
import com.cardpaymentsystem.security.jwt.JwtUtils;
import com.cardpaymentsystem.security.services.UserDetailsImpl;
import com.cardpaymentsystem.security.services.UserDetailsServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserDetailsServiceImpl userDetailsServiceImpl;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
				userDetails.getAge(), userDetails.getEmail(), userDetails.getAddress(), userDetails.getPhoneNumber()));// roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userDetailsServiceImpl.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userDetailsServiceImpl.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getAge(), signUpRequest.getEmail(),
				signUpRequest.getAddress(), signUpRequest.getPhoneNumber(),
				encoder.encode(signUpRequest.getPassword()));

		userDetailsServiceImpl.save(user);
		// return new ResponseEntity<>(HttpStatus.CREATED);
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@PutMapping("/update")
	public ResponseEntity<?> updateUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userDetailsServiceImpl.existsByUsername(signUpRequest.getUsername())
				&& userDetailsServiceImpl.existsByEmail(signUpRequest.getEmail())) {
			// Update existing user's account
			User user1 = userDetailsServiceImpl.findByEmail(signUpRequest.getEmail());
			Long id = user1.getId();
			User user = new User(id, signUpRequest.getUsername(), signUpRequest.getAge(), signUpRequest.getEmail(),
					signUpRequest.getAddress(), signUpRequest.getPhoneNumber(),
					encoder.encode(signUpRequest.getPassword()));

			userDetailsServiceImpl.save(user);

			return ResponseEntity.ok(new MessageResponse("User updated successfully!"));
		}

		return ResponseEntity.badRequest().body(new MessageResponse("Error: Username and Email is already taken!"));

	}
}