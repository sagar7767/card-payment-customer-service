package com.cardpaymentsystem.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.cardpaymentsystem.security.services.UserDetailsImpl;

@Entity
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "email") })
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String username;

	// added
//	@NotBlank
//	@Size(max = 2)
	private int age;

	@NotBlank
	@Size(max = 50)
	@Email
	private String email;

	// added
//	@NotBlank
//	@Size(max = 100)
	private String address;

	// added
//	@NotBlank
//	@Size(max = 10)
	private long phoneNumber;

	@NotBlank
	@Size(max = 120)
	private String password;

	/*
	 * @ManyToMany(fetch = FetchType.LAZY)
	 * 
	 * @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"),
	 * inverseJoinColumns = @JoinColumn(name = "role_id")) private Set<Role> roles =
	 * new HashSet<>();
	 */
	public User() {
	}

	public User(String username, int age, String email, String address, long phoneNumber, String password) {
		this.username = username;
		this.age = age;
		this.email = email;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.password = password;

	}
	
	public User(Long id,String username, int age, String email, String address, long phoneNumber, String password) {
		this.id=id;
		this.username = username;
		this.age = age;
		this.email = email;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.password = password;

	}
	
	public static User build(User user) {
		
		return new User(user.getId(), user.getUsername(), user.getAge(), user.getEmail(), user.getAddress(),
				user.getPhoneNumber(), user.getPassword());//, authorities);
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

//	public Set<Role> getRoles() {
//		return roles;
//	}
//
//	public void setRoles(Set<Role> roles) {
//		this.roles = roles;
//	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", age=" + age + ", email=" + email + ", address="
				+ address + ", phoneNumber=" + phoneNumber + ", password=" + password + "]";
	}

}